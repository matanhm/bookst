<?php

use Illuminate\Database\Seeder;

class BookstTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
[
	[
            'title' => 'Book1',
            'author' => 'Author1',
            'user_id' => '1',
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'Book2',
            'author' => 'Author2',
            'user_id' => '2',
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'Book3',
            'author' => 'Author3',
            'user_id' => '1',
            'created_at' => date('Y-m-d G:i:s'),
	],

        ]);
    }

}
