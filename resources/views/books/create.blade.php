@yield('content')
@extends('layouts.app')

<h1>Create New Book</h1>
<form method = 'post' action="{{action('BookstController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "title">What book would you like to add?</label><br>
   <a>Title  </a> <input type= "text" class = "form-control" name= "title"><br><br>
   <a>Author</a> <input type= "text" class = "form-control" name= "author">
</div>
<br>
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just read it">
</div>

</form>
<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

@section('content')
@endsection