@yield('content')
@extends('layouts.app')

<h1>Edit a Book to Read</h1>
<form method = 'post' action="{{action('BookstController@update', $book->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">A Book to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$book->title}}">
    <input type= "text" class = "form-control" name= "author" value = "{{$book->author}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update Your Book">
</div>
</form>
<form method = 'post' action="{{action('BookstController@destroy', $book->id)}}">
@csrf
@method('DELETE')

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Book">
</div>
</form>

@section('content')
@endsection