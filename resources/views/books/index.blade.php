@yield('content')
@extends('layouts.app')
@section('content')
<h1> This is Your Books List</h1>

<table style="width:25%">
  <tr>
    <th>Book</th>
    <th>Author</th> 
    <th>Status</th>
  </tr>
  @foreach($books as $book)
  <tr>
    <td>@cannot('reader')<a href= "{{route('books.edit', $book->id )}}">@endcannot {{$book->title}} </a></td>
    <td>@cannot('reader')<a href= "{{route('books.edit', $book->id )}}">@endcannot {{$book->author}} </a></td> 
   <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif
    </td>
  </tr>
  @endforeach
</table>

@cannot('reader')<a href= "{{route('books.create')}}">@endcannot Create a new Book</a>



<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>  
   

@endsection